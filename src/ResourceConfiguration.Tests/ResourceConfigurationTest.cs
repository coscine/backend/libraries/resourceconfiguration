﻿using Coscine.ResourceTypeBase;
using NUnit.Framework;

namespace Coscine.ResourceConfiguration.Tests
{
    [TestFixture]
    public class ResourceConfigurationTest
    {
        private static readonly string _resourceTypeValid = "linked";
        private static readonly string _resourceTypeInvalid = "sdsds";

        public ResourceConfigurationTest()
        {

        }

        [OneTimeSetUp]
        public void Setup()
        {

        }

        [OneTimeTearDown]
        public void End()
        {

        }

        [Test]
        public void TestConstructor()
        {
            _ = new ResourceConfiguration();
        }

        [Test]
        public void TestIsValidResource()
        {
            ResourceConfiguration resourcenConfiguration = new ResourceConfiguration();
            Assert.True(resourcenConfiguration.IsValidResource(_resourceTypeValid));
            Assert.False(resourcenConfiguration.IsValidResource(_resourceTypeInvalid));
        }

        [Test]
        public void TestGetResourceType()
        {
            ResourceConfiguration resourcenConfiguration = new ResourceConfiguration();
            Assert.True(resourcenConfiguration.GetResourceType(_resourceTypeValid).GetType() == new ResourceTypeConfigurationObject().GetType());
            Assert.True(resourcenConfiguration.GetResourceType(_resourceTypeInvalid) == null);
        }

        [Test]
        public void TestGetResourceTypes()
        {
            ResourceConfiguration resourcenConfiguration = new ResourceConfiguration();
            Assert.IsTrue(resourcenConfiguration.GetResourceTypesByStatus("active").Count == resourcenConfiguration.GetAvailableResourceTypes().Count);
        }


    }
}
