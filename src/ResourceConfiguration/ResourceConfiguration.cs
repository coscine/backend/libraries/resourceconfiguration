﻿using Coscine.ResourceTypeBase;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Coscine.ResourceConfiguration
{
    public class ResourceConfiguration
    {
        private readonly Dictionary<string, ResourceTypeConfigurationObject> _types = new Dictionary<string, ResourceTypeConfigurationObject>();
        public static string EmbeddedResourceTypesPath { get; } = "Coscine.ResourceConfiguration.ResourceTypes.json";

        public ResourceConfiguration()
        {
            foreach (var kv in GetJSONFromAssembly(EmbeddedResourceTypesPath))
            {
                _types.Add(kv.Key, kv.Value.ToObject<ResourceTypeConfigurationObject>());
            }
        }

        public static JObject GetJSONFromAssembly(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream(fileName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string result = reader.ReadToEnd();
                    return (JObject)JsonConvert.DeserializeObject(result);
                }
            }
        }

        public bool IsValidResource(string resourceType)
        {
            return _types.ContainsKey(resourceType);
        }

        public ResourceTypeConfigurationObject GetResourceType(string type)
        {
            if (IsValidResource(type))
            {
                return _types[type];
            }
            return null;
        }

        public List<string> GetResourceTypesByStatus(string status)
        {
            var list = new List<string>();
            foreach (var key in _types.Keys)
            {
                if (_types[key].Status == status)
                {
                    list.Add(key);
                }
            }
            return list;
        }

        public List<string> GetAvailableResourceTypes()
        {
            return GetResourceTypesByStatus("active");
        }

    }
}
